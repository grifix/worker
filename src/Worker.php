<?php
declare(strict_types=1);

namespace Grifix\Worker;

use DateTimeImmutable;
use Grifix\Clock\ClockInterface;
use Grifix\Memory\MemoryInterface;
use Psr\Log\LoggerInterface;

final class Worker implements WorkerInterface
{
    private DateTimeImmutable $startDate;

    private int $failsCounter = 0;

    private int $runCounter = 0;

    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ClockInterface $clock,
        private readonly MemoryInterface $memory,
        private readonly ?int $failsLimit = 5,
        private readonly int $memoryLimit = 100 * MemoryInterface::MB,
        private readonly ?int $timeLimit = null,
        private readonly ?int $iterationDelay = 100000,
        private readonly ?int $runLimit = null
    )
    {
    }

    public function run(callable $callback): void
    {
        $this->startDate = $this->clock->getCurrentTime();
        while (true) {
            if ($this->hasMemoryLimitExceed()) {
                return;
            }

            if ($this->hasTimeLimitExceed()) {
                return;
            }

            if ($this->hasFailsLimitExceed()) {
                return;
            }

            if ($this->hasRunLimitExceed()) {
                return;
            }

            try {
                $this->runCounter++;
                $callback();
                if(null !== $this->iterationDelay){
                    usleep($this->iterationDelay);
                }
            } catch (\Throwable $exception) {
                $this->handleFail($exception);
            }
        }
    }

    private function hasMemoryLimitExceed(): bool
    {
        return $this->memory->getUsage() > $this->memoryLimit;
    }

    private function hasRunLimitExceed(): bool
    {
        if (null === $this->runLimit) {
            return false;
        }
        return $this->runCounter >= $this->runLimit;
    }

    private function hasTimeLimitExceed(): bool
    {
        if (null === $this->timeLimit) {
            return false;
        }
        return $this->clock->getCurrentTime()->getTimestamp() - $this->startDate->getTimestamp() > $this->timeLimit;
    }

    private function hasFailsLimitExceed(): bool
    {
        if (null === $this->failsLimit) {
            return false;
        }

        return $this->failsCounter >= $this->failsLimit;
    }

    private function handleFail(\Throwable $throwable): void
    {
        $this->failsCounter++;
        $this->logger->error($throwable->getMessage(), [
            'file' => $throwable->getFile(),
            'line' => $throwable->getLine(),
            'code' => $throwable->getCode(),
            'trace' => $throwable->getTraceAsString(),
        ]);
    }
}
