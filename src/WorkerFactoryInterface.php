<?php
declare(strict_types=1);

namespace Grifix\Worker;

use Grifix\Memory\MemoryInterface;

interface WorkerFactoryInterface
{
    public function create(
        int $failsLimit = 5,
        int $memoryLimit = 100 * MemoryInterface::MB,
        int $timeLimit = null,
        int $iterationDelay = 100000,
        int $runLimit = null
    ): WorkerInterface;
}
