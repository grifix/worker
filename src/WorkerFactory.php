<?php
declare(strict_types=1);

namespace Grifix\Worker;

use Grifix\Clock\ClockInterface;
use Grifix\Clock\SystemClock;
use Grifix\Memory\MemoryInterface;
use Grifix\Memory\SystemMemory;
use Psr\Log\LoggerInterface;

final class WorkerFactory implements WorkerFactoryInterface
{

    public function __construct(
        private readonly ClockInterface $clock,
        private readonly MemoryInterface $memory,
        private readonly LoggerInterface $logger
    )
    {
    }

    public function create(
        int  $failsLimit = 5,
        int  $memoryLimit = 100 * MemoryInterface::MB,
        ?int  $timeLimit = null,
        int  $iterationDelay = 100000,
        ?int $runLimit = null
    ): WorkerInterface
    {
        return new Worker(
            $this->logger,
            $this->clock,
            $this->memory,
            $failsLimit,
            $memoryLimit,
            $timeLimit,
            $iterationDelay,
            $runLimit
        );
    }
}
