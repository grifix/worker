<?php

declare(strict_types=1);

namespace Grifix\Worker;

use Grifix\Clock\ClockInterface;
use Grifix\Memory\MemoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractWorkerCommand extends Command
{
    public const OPT_MEMORY_LIMIT = 'memory-limit';

    public const OPT_TIME_LIMIT = 'time-limit';

    public const OPT_FAILS_LIMIT = 'fails-limit';

    public const OPT_ITERATION_DELAY = 'iteration-delay';

    public const OPT_RUN_LIMIT = 'run-limit';

    public function __construct(private readonly WorkerFactoryInterface $workerFactory)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        parent::configure();
        $this->addOption(self::OPT_FAILS_LIMIT, null, InputOption::VALUE_OPTIONAL, 'maximum number of fails', 5)
             ->addOption(
                 self::OPT_MEMORY_LIMIT,
                 null,
                 InputOption::VALUE_OPTIONAL,
                 'memory limit in bytes',
                 100 * MemoryInterface::MB
             )
             ->addOption(
                 self::OPT_TIME_LIMIT,
                 null,
                 InputOption::VALUE_OPTIONAL,
                 'time limit in seconds',
                 30 * ClockInterface::MINUTE
             )
             ->addOption(
                 self::OPT_ITERATION_DELAY,
                 null,
                 InputOption::VALUE_OPTIONAL,
                 'iteration delay in microseconds',
                 100000
             )
             ->addOption(
                 self::OPT_RUN_LIMIT,
                 null,
                 InputOption::VALUE_OPTIONAL,
                 'maximum number of worker executions'
             );;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $runLimit = $input->getOption(self::OPT_RUN_LIMIT);
        if (null !== $runLimit) {
            $runLimit = (int)$runLimit;
        }
        $this->workerFactory->create(
            (int)$input->getOption(self::OPT_FAILS_LIMIT),
            (int)$input->getOption(self::OPT_MEMORY_LIMIT),
            (int)$input->getOption(self::OPT_TIME_LIMIT),
            (int)$input->getOption(self::OPT_ITERATION_DELAY),
            $runLimit
        )->run($this->getCallback($input, $output));

        return Command::SUCCESS;
    }

    abstract protected function getCallback(InputInterface $input, OutputInterface $output): callable;
}
