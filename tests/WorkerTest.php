<?php
declare(strict_types=1);

namespace Grifix\Worker\Tests;

use Grifix\Clock\ClockInterface;
use Grifix\Clock\FrozenClock;
use Grifix\Clock\SystemClock;
use Grifix\Memory\MemoryInterface;
use Grifix\Memory\SystemMemory;
use Grifix\Worker\Worker;
use Mockery\Mock;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

final class WorkerTest extends TestCase
{
    private array $errors = [];


    private int $runCounter = 0;

    protected function setUp(): void
    {
        parent::setUp();
    }


    public function testItAppliesRunLimit(): void
    {
        $worker = new Worker(
            $this->createLogger(),
            new SystemClock(),
            new SystemMemory(),
            failsLimit: null,
            iterationDelay: null,
            runLimit: 5
        );
        $worker->run($this->createCallback());
        self::assertEquals(5, $this->runCounter);
    }

    public function testItAppliesMemoryLimit(): void
    {
        $memory = $this->createMemory(101);
        $worker = new Worker(
            $this->createLogger(),
            new FrozenClock(),
            $memory,
            memoryLimit: 100,
        );
        $worker->run($this->createCallback());
        self::assertEquals(0, $this->runCounter);
    }

    public function testItAppliesTimeLimit(): void
    {
        /** @var ClockInterface|Mock $clock */
        $clock = \Mockery::mock(ClockInterface::class);
        $clock->shouldReceive('getCurrentTime')->andReturn(
            new \DateTimeImmutable('2001-01-01 00:00:00'),
            new \DateTimeImmutable('2001-01-01 00:00:05'),
            new \DateTimeImmutable('2001-01-01 00:00:11')
        );
        $worker = new Worker(
            $this->createLogger(),
            $clock,
            new SystemMemory(),
            timeLimit: 10,
        );
        $worker->run($this->createCallback());
        self::assertEquals(1, $this->runCounter);
    }

    public function testItAppliesFailsLimit(): void
    {
        $worker = new Worker(
            $this->createLogger(),
            new SystemClock(),
            new SystemMemory(),
            failsLimit: 3,
        );

        $worker->run($this->createFailingCallback());
        self::assertEquals(3, $this->runCounter);
        self::assertCount(3, $this->errors);
    }

    private function createCallback(): callable
    {
        return function () {
            $this->runCounter++;
        };
    }

    private function createFailingCallback(): callable
    {
        return function () {
            $this->runCounter++;
            throw new \Exception('error');
        };
    }

    private function createLogger(): LoggerInterface
    {
        /** @var LoggerInterface $result */
        $result = \Mockery::mock(LoggerInterface::class)
            ->shouldReceive('error')
            ->andReturnUsing(
                function (string $message, array $context) {
                    $this->errors[] = [
                        'message' => $message,
                        'context' => $context
                    ];
                }
            )->getMock();

        return $result;
    }

    private function createMemory(int $usage = MemoryInterface::GB): MemoryInterface
    {
        /** @var MemoryInterface $result */
        $result = \Mockery::mock(MemoryInterface::class)
            ->shouldReceive('getUsage')
            ->andReturn($usage)
            ->getMock();

        return $result;
    }
}
